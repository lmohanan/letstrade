package com.hacksc.letstrade;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.Random;

import org.json.JSONObject;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.MatrixCursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

/**
 * An example full-screen activity that shows the home page.
 * 
 */
public class MainScreen extends Activity {
	static ArrayList<String> list = new ArrayList<String>();
	static MatrixCursor tradeListCursor = null;
	static MatrixCursor wishListCursor = null;
	
	static {
		list.add("1");
		list.add("2");
		list.add("3");
		list.add("4");
	}
	
	static Random random = new Random();
	
	public static String emailAddress;
	public static String points;
	public static String uid;
	public static String username;
	
	public static String profileImageId;
	public static String profileImageUri;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.home_page);
		
		Button login = (Button) findViewById(R.id.login);
		login.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				Intent intent = new Intent(getApplicationContext(), BrowseTradeListActivity.class);
				startActivity(intent);
			}
		});

	}
	
	@Override
	protected void onResume() {
		new LoadUserDetails(getApplicationContext()).execute();
		super.onResume();
	}

	private static class LoadUserDetails extends AsyncTask<String, Void, String> {
		Context context;
		public LoadUserDetails(Context applicationContext) {
			context = applicationContext;
		}

		protected void onPreExecute(){ 
			super.onPreExecute();
        }
		
		@Override
		protected String doInBackground(String... params) {
			try{
				URL url = new URL("http://107.170.251.126:8080/letstrade/rest/user/"+list.get(random.nextInt(list.size())));
	
				StringBuilder buffer = new StringBuilder();
		        BufferedReader in = new BufferedReader(new InputStreamReader(url.openStream()));
	
		        String inputLine;
		        while ((inputLine = in.readLine()) != null)
		            buffer.append(inputLine);
				in.close();
				
				JSONObject obj = new JSONObject(buffer.toString());
				points = obj.getString("points");
				profileImageId = obj.getString("profileImageId");
				profileImageUri = obj.getString("profileImageUri");
				emailAddress = obj.getString("emailAddress");
				
				JSONObject tradeList = new JSONObject(obj.getString("tradeList"));
				String projection[] = new String[]{"_id","image", "title", "description", "points", "userId", "itemId"};
				tradeListCursor = new MatrixCursor(projection, 1);
				String imageName[] = null;
				imageName = tradeList.getString("picUri").split("/");
		        
				File file = context.getFileStreamPath(imageName[imageName.length - 1]);
				URL uri;
				Bitmap bitmap = null;
				FileOutputStream fos;
				if(!file.exists()) {
					uri = new URL(tradeList.getString("picUri"));
					bitmap = BitmapFactory.decodeStream(uri.openConnection().getInputStream());
					fos = context.openFileOutput(imageName[imageName.length-1], Context.MODE_PRIVATE); 
			        bitmap.compress(Bitmap.CompressFormat.PNG, 100, fos); 
			        fos.close();
				}
		        tradeListCursor.addRow(new Object[] { 0,
						imageName[imageName.length - 1],
						tradeList.getString("title"),
						tradeList.getString("description"),
						tradeList.getString("points"),
						tradeList.getString("userId"),
						tradeList.getString("itemId")});
				
				wishListCursor = new MatrixCursor(projection, 1);
				JSONObject item = tradeList = new JSONObject(obj.getString("wishList"));;
				imageName = item.getString("picUri").split("/");
				file = context.getFileStreamPath(imageName[imageName.length - 1]);
				if(!file.exists()) {
					uri = new URL(item.getString("picUri"));
					bitmap = BitmapFactory.decodeStream(uri.openConnection().getInputStream());
			        fos = context.openFileOutput(imageName[imageName.length-1], Context.MODE_PRIVATE); 
			        bitmap.compress(Bitmap.CompressFormat.PNG, 100, fos); 
			        fos.close();
				}
		        wishListCursor.addRow(new Object[] { 0,
						imageName[imageName.length - 1],
						item.getString("title"),
						"",
						item.getString("points"),
						item.getString("userId"),
						item.getString("itemId")});
			} catch (Exception e) {
				e.printStackTrace();
			}
			return null;
		}
		
		protected void onPostExecute(String cursor) {
		}
	}
}
