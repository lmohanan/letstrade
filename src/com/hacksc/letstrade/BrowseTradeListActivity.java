package com.hacksc.letstrade;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;

public class BrowseTradeListActivity extends Activity {
	private ArrayList<String> menu = new ArrayList<String>();
	private static DrawerLayout mDrawerLayout;
	private static ListView mDrawerList, tradeList;
	private android.support.v4.app.ActionBarDrawerToggle mDrawerToggle;
	private CharSequence mDrawerTitle;
	private CharSequence mTitle;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.drawer_layout);

		menu.add("Offers");
		menu.add("Items on Sale");
		menu.add("My points");
		menu.add("I have..");
		menu.add("I want..");
		menu.add("Offers I made");

		mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
		// set a custom shadow that overlays the main content when the drawer
		// opens
		mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow,
				GravityCompat.START);

		mDrawerList = (ListView) mDrawerLayout.findViewById(R.id.left_drawer);
		ItemAdapter item = new ItemAdapter(this, R.layout.drawer_list_item,
				menu);
		mDrawerList.setAdapter(item);
		mDrawerList.setOnItemClickListener(new DrawerItemClickListener());

		// enable ActionBar app icon to behave as action to toggle nav drawer
		getActionBar().setDisplayHomeAsUpEnabled(true);

		mTitle = mDrawerTitle = getTitle();

		mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout,
				R.drawable.ic_drawer, R.string.drawer_open,
				R.string.drawer_close) {
			public void onDrawerClosed(View view) {
				getActionBar().setTitle(mTitle);
				invalidateOptionsMenu(); // creates call to
											// onPrepareOptionsMenu()
			}

			public void onDrawerOpened(View drawerView) {
				getActionBar().setTitle(mDrawerTitle);
				invalidateOptionsMenu(); // creates call to
											// onPrepareOptionsMenu()
			}
		};

		mDrawerLayout.setDrawerListener(mDrawerToggle);
	}

	@Override
	protected void onStart() {
		selectItem(1);
		super.onResume();
	}
	
	private static class LoadTradeList extends AsyncTask<String, Void, Cursor> {
		private ProgressDialog pdia;
		private Context context;
		
		public LoadTradeList(Context context) {
			this.context = context;
		}
		protected void onPreExecute(){ 
			super.onPreExecute();
			pdia = new ProgressDialog(context);
			pdia.setMessage("Fetching items...");
			pdia.show();
        }
		@Override
		protected Cursor doInBackground(String... params) {
			MatrixCursor cursor = null;
			try{
				URL url = new URL("http://107.170.251.126:8080/letstrade/rest/user/allitems");
	
				StringBuilder buffer = new StringBuilder();
		        BufferedReader in = new BufferedReader(new InputStreamReader(url.openStream()));
	
		        String inputLine;
		        while ((inputLine = in.readLine()) != null)
		            buffer.append(inputLine);
				in.close();
				JSONArray results = new JSONArray(new JSONObject(buffer.toString()).getString("item"));
		        String projection[] = new String[]{"_id","image", "title", "description", "points", "userId", "itemId"};
				cursor = new MatrixCursor(projection, results.length());
				
				for(int i=0;i<results.length();++i) {
					JSONObject item = (JSONObject) results.get(i);
					if(item.getString("status").equalsIgnoreCase("haves")) {
						String imageName[] = null;
						imageName = item.getString("picUri").split("/");
						File file = context.getFileStreamPath(imageName[imageName.length - 1]);
						if(!file.exists()) {
					        URL uri;
							Bitmap bitmap = null;
							FileOutputStream fos;
							uri = new URL(item.getString("picUri"));
							bitmap = BitmapFactory.decodeStream(uri.openConnection().getInputStream());
							fos = context.openFileOutput(imageName[imageName.length-1], Context.MODE_PRIVATE); 
					        bitmap.compress(Bitmap.CompressFormat.PNG, 100, fos); 
					        fos.close();
						}
						cursor.addRow(new Object[] { i + "",
								imageName[imageName.length - 1],
								item.getString("title"),
								item.getString("description"),
								item.getString("points"),
								item.getString("userId"),
								item.getString("itemId")});
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			return cursor;
		}
		
		protected void onPostExecute(Cursor cursor) {
			if (pdia.isShowing())
				pdia.dismiss();
			ListCursorAdapter adapter = new ListCursorAdapter(context,
					R.layout.browse_trade_list_item, cursor, new String[] {
							"image", "title", "description", "points" },
					new int[] { R.id.browse_trade_item_pic,
							R.id.trade_item_title, R.id.trade_item_desc,
							R.id.trade_item_points }, 0);
			tradeList.setAdapter(adapter);
		}
	}
	
	private static class ListCursorAdapter extends SimpleCursorAdapter {
		Context context;
		Cursor cursor;
		public ListCursorAdapter(Context context, int layout, Cursor c,
				String[] from, int[] to, int flags) {
			super(context, layout, c, from, to, flags);
			this.context = context;
			cursor = c;
		}
		
		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			convertView = super.getView(position, convertView, parent);
			final String title = cursor.getString(2);
			final String desc = cursor.getString(3);
			final String points = cursor.getString(4);
			
			TextView titleView = (TextView)convertView.findViewById(R.id.trade_item_title);
			TextView descView = (TextView)convertView.findViewById(R.id.trade_item_desc);
			TextView pointsView = (TextView)convertView.findViewById(R.id.trade_item_points);
			
			titleView.setText(title);
			descView.setText(desc);
			pointsView.setText(points);
			
			ImageView img = (ImageView)convertView.findViewById(R.id.browse_trade_item_pic);
			Bitmap bitmap;
			try {
				bitmap = BitmapFactory.decodeStream(context.openFileInput(cursor.getString(1)));
				img.setImageBitmap(bitmap);
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}
			
			Button make_offer_btn = (Button)convertView.findViewById(R.id.make_offer_btn);
			make_offer_btn.setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View v) {
					Intent makeOffer = new Intent(context, SelectItemsToOfferActivity.class);
					makeOffer.putExtra("receiverUid", cursor.getString(5));
					makeOffer.putExtra("itemId", cursor.getString(5));
					context.startActivity(makeOffer);
				}
			});
			return convertView;
		}
	}
	private class DrawerItemClickListener implements
			ListView.OnItemClickListener {

		@Override
		public void onItemClick(AdapterView<?> parent, View view, int pos,
				long id) {
			selectItem(pos);
		}
	}
	
	/** Swaps fragments in the main content view */
	private void selectItem(int position) {
		// Create a new fragment and specify the planet to show based on
		// position
		Fragment fragment = new MyFragment();
		Bundle args = new Bundle();
		args.putInt(MyFragment.ARG_PLANET_NUMBER, position);
		fragment.setArguments(args);

		// Insert the fragment by replacing any existing fragment
		FragmentManager fragmentManager = getFragmentManager();
		fragmentManager.beginTransaction()
				.replace(R.id.content_frame, fragment).commit();

		// Highlight the selected item, update the title, and close the
		// drawer
		mDrawerList.setItemChecked(position, true);
		// setTitle(menu[position]);
		mDrawerLayout.closeDrawer(mDrawerList);
	}
	
	
	/**
     * Fragment that appears in the "content_frame", shows a planet
     */
    public static class MyFragment extends Fragment {
        public static final String ARG_PLANET_NUMBER = "request_number";

        public MyFragment() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                Bundle savedInstanceState) {
            View rootView = null;
            int pos = getArguments().getInt(ARG_PLANET_NUMBER);
            
            switch(pos) {
	            case 3:
	            	rootView = inflater.inflate(R.layout.i_have_list, container, false);
	            	RelativeLayout haveLay = (RelativeLayout)rootView.findViewById(R.id.haveLay);
	            	ListView haveList = (ListView)haveLay.findViewById(R.id.i_have_list_view);
	            	ListCursorAdapter adapter = new ListCursorAdapter(getActivity(),
	    					R.layout.browse_trade_list_item, MainScreen.tradeListCursor, new String[] {
	    							"image", "title", "description", "points" },
	    					new int[] { R.id.browse_trade_item_pic,
	    							R.id.trade_item_title, R.id.trade_item_desc,
	    							R.id.trade_item_points }, 0);
	            	haveList.setAdapter(adapter);
	            	break;
	            case 4:
	            	rootView = inflater.inflate(R.layout.i_want_list, container, false);
	            	RelativeLayout wantLay = (RelativeLayout)rootView.findViewById(R.id.wantLay);
	            	ListView wishList = (ListView)wantLay.findViewById(R.id.i_want_list_view);
	            	ListCursorAdapter adapter1 = new ListCursorAdapter(getActivity(),
	    					R.layout.browse_trade_list_item, MainScreen.wishListCursor, new String[] {
	    							"image", "title", "description", "points" },
	    					new int[] { R.id.browse_trade_item_pic,
	    							R.id.trade_item_title, R.id.trade_item_desc,
	    							R.id.trade_item_points }, 0);
	            	wishList.setAdapter(adapter1);
	            	break;
            	case 2:
            		rootView = inflater.inflate(R.layout.my_points, container, false);
            		TextView my_points = (TextView)rootView.findViewById(R.id.my_points);
            		my_points.setText(MainScreen.points);
            		break;
	            case 1:
	            	rootView = inflater.inflate(R.layout.browse_trade_list, container, false);
	            	tradeList = (ListView)rootView.findViewById(R.id.trade_list);
	        		new LoadTradeList(getActivity()).execute("");
	            	break;
            }
            
            return rootView;
        }
    }
}
