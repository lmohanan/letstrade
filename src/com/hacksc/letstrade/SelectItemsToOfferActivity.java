package com.hacksc.letstrade;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.ActionMode;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Checkable;
import android.widget.FrameLayout;
import android.widget.GridView;
import android.widget.ImageView;

public class SelectItemsToOfferActivity extends Activity {
	GridView mGrid;
	List<String> haves = new ArrayList<String>();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.pick_grid);
		
		Intent mainIntent = new Intent(Intent.ACTION_MAIN, null);
        mainIntent.addCategory(Intent.CATEGORY_LAUNCHER);

		mGrid = (GridView) findViewById(R.id.item_pick_grid);
		mGrid.setAdapter(new AppsAdapter());
		mGrid.setChoiceMode(GridView.CHOICE_MODE_MULTIPLE);
		mGrid.setMultiChoiceModeListener(new MultiChoiceModeListener());
		
		for(int i=0;i<MainScreen.tradeListCursor.getCount();i++) {
			haves.add(""+i);
		}
	}

	public class MultiChoiceModeListener implements
			GridView.MultiChoiceModeListener {
		public boolean onCreateActionMode(ActionMode mode, Menu menu) {
			mode.setTitle("Select Items");
			mode.setSubtitle("One item selected");
			return true;
		}
		
		public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
			return true;
		}

		public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
			return true;
		}

		public void onDestroyActionMode(ActionMode mode) {
		}

		public void onItemCheckedStateChanged(ActionMode mode, int position,
				long id, boolean checked) {
			int selectCount = mGrid.getCheckedItemCount();
			switch (selectCount) {
			case 1:
				mode.setSubtitle("One item selected");
				break;
			default:
				mode.setSubtitle("" + selectCount + " items selected");
				break;
			}
		}

	}

	public class AppsAdapter extends BaseAdapter {
		public AppsAdapter() {
		}

		public View getView(int position, View convertView, ViewGroup parent) {
			CheckableLayout l;
			ImageView i;

			if (convertView == null) {
				i =  new ImageView(SelectItemsToOfferActivity.this);
				i.setScaleType(ImageView.ScaleType.FIT_CENTER);
				i.setLayoutParams(new ViewGroup.LayoutParams(100, 100));
				
				l = new CheckableLayout(SelectItemsToOfferActivity.this);
				l.setLayoutParams(new GridView.LayoutParams(
						GridView.LayoutParams.WRAP_CONTENT,
						GridView.LayoutParams.WRAP_CONTENT));
				l.addView(i);
			} else {
				l = (CheckableLayout) convertView;
				i = (ImageView) l.getChildAt(0);
			}

			MainScreen.tradeListCursor.moveToPosition(position);
			Bitmap bitmap;
			try {
				bitmap = BitmapFactory
						.decodeStream(getApplicationContext().openFileInput(
								MainScreen.tradeListCursor.getString(1)));
				i.setImageBitmap(bitmap);
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}

			return l;
		}

		public final int getCount() {
			return haves.size();
		}

		public final Object getItem(int position) {
			return haves.get(position);
		}

		public final long getItemId(int position) {
			return position;
		}
	}

	public class CheckableLayout extends FrameLayout implements Checkable {
		private boolean mChecked;

		public CheckableLayout(Context context) {
			super(context);
		}

		public void setChecked(boolean checked) {
			mChecked = checked;
			if(checked)
				setBackgroundResource(R.drawable.lets_draw_ribbon)/*BackgroundDrawable(checked ? getResources().getDrawable(
					R.drawable.lets_draw_ribbon) : null)*/;
		}

		public boolean isChecked() {
			return mChecked;
		}

		public void toggle() {
			setChecked(!mChecked);
		}

	}
	
	public void submit(View view) {
		mGrid.getSelectedItemPosition();
	}
}
